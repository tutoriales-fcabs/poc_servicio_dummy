package com.casaideas.kit.srvdummy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocServicioDummyApplication {

	public static void main(String[] args) {
		SpringApplication.run(PocServicioDummyApplication.class, args);
	}

}
