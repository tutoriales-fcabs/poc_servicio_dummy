package com.casaideas.kit.srvdummy.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.casaideas.kit.srvdummy.model.Respuesta;
import com.casaideas.kit.srvdummy.services.DelayService;
import com.casaideas.kit.srvdummy.services.ResponseService;

@RestController
public class DummyController {
	
	private final static Logger logger = LoggerFactory.getLogger(DummyController.class);
	
	@Autowired
	private ResponseService responseSrv;
	
	@Autowired
	private DelayService delaySrv;
	
	@GetMapping("/api/saludo")
	public Respuesta saludo(@RequestParam(required = false) String nombre, @RequestParam long delay) {
		
		Respuesta respuesta = new Respuesta();
		
		try {
			if(nombre == null) {
				respuesta.setMensaje(responseSrv.saludo());
			}
			else {
				respuesta.setMensaje(responseSrv.saludo(nombre));
			}
			
			delaySrv.delay(delay);
		}
		catch(Exception ex) {
			logger.error(ex.getMessage());
		}
		
		return respuesta;
	}
	
}
