package com.casaideas.kit.srvdummy.model;

public class Respuesta {
	private String mensaje;

	public Respuesta() {
		super();
	}

	public Respuesta(String mensaje) {
		super();
		this.mensaje = mensaje;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}
