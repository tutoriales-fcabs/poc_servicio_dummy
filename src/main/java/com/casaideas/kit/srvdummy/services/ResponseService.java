package com.casaideas.kit.srvdummy.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ResponseService {
	
	@Value("${app.saludo.template:Hola %s!}")
	private String saludo_template;
	
	public String saludo(String nombre) {
		return String.format(saludo_template, nombre);
	}
	
	public String saludo() {
		return String.format(saludo_template, "mundo");
	}
	
}
