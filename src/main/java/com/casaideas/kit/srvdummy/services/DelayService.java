package com.casaideas.kit.srvdummy.services;

import org.springframework.stereotype.Service;

@Service
public class DelayService {
	public void delay(long ms) throws InterruptedException {
		Thread.sleep(ms);
	}
}
